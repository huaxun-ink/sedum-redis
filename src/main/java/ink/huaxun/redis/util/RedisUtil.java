package ink.huaxun.redis.util;

import ink.huaxun.util.SpringContextUtil;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author zhaogang
 * @description redis工具类
 * @date 2023/6/15 17:43
 */
public class RedisUtil {

    /**
     * 获取redisTemplate bean
     * 因为class不能传入泛型，所以采用强转并清除警告
     */
    @SuppressWarnings("unchecked")
    private static final RedisTemplate<String, Object> REDIS_TEMPLATE = (RedisTemplate<String, Object>) SpringContextUtil.getBean("redisTemplate", RedisTemplate.class);

    /**
     * 指定缓存失效时间
     */
    public static void setExpire(String key, long seconds) {
        REDIS_TEMPLATE.expire(key, seconds, TimeUnit.SECONDS);
    }

    /**
     * 根据key获取过期时间，返回0代表为永久有效
     */
    public static Long getExpire(String key) {
        return REDIS_TEMPLATE.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     */
    public static Boolean has(String key) {
        return REDIS_TEMPLATE.hasKey(key);
    }

    /**
     * 删除缓存
     */
    public static void delete(String... key) {
        if (key == null || key.length <= 0) {
            return;
        }
        REDIS_TEMPLATE.delete(Arrays.asList(key));
    }

    /**
     * 删除集合对象
     */
    public static void delete(Collection<String> collection) {
        if (collection == null || collection.size() <= 0) {
            return;
        }
        REDIS_TEMPLATE.delete(collection);
    }

    /**
     * 普通缓存获取
     */
    public static Object get(String key) {
        return key == null ? null : REDIS_TEMPLATE.opsForValue().get(key);
    }

    /**
     * 普通缓存获取
     */
    public static String getString(String key) {
        Object value = RedisUtil.get(key);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * 普通缓存获取
     */
    public static Integer getInteger(String key) {
        Object value = RedisUtil.get(key);
        if (value == null) {
            return null;
        }
        return Integer.valueOf(value.toString());
    }

    /**
     * 普通缓存获取
     */
    public static Long getLong(String key) {
        Object value = RedisUtil.get(key);
        if (value == null) {
            return null;
        }
        return Long.valueOf(value.toString());
    }

    /**
     * 普通缓存获取
     */
    public static Float getFloat(String key) {
        Object value = RedisUtil.get(key);
        if (value == null) {
            return null;
        }
        return Float.valueOf(value.toString());
    }

    /**
     * 普通缓存获取
     */
    public static Double getDouble(String key) {
        Object value = RedisUtil.get(key);
        if (value == null) {
            return null;
        }
        return Double.valueOf(value.toString());
    }

    /**
     * 普通缓存放入
     */
    public static void set(String key, Object value) {
        REDIS_TEMPLATE.opsForValue().set(key, value);
    }

    /**
     * 普通缓存放入并设置时间
     */
    public static void set(String key, Object value, long seconds) {
        if (seconds > 0) {
            REDIS_TEMPLATE.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
            return;
        }
        RedisUtil.set(key, value);
    }

    /**
     * 递增
     */
    public static Long increment(String key) {
        return REDIS_TEMPLATE.opsForValue().increment(key);
    }

    /**
     * 递增
     */
    public static Long increment(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForValue().increment(key, delta);
    }

    /**
     * 递增
     */
    public static Double increment(String key, double delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     */
    public static Long decrement(String key) {
        return REDIS_TEMPLATE.opsForValue().decrement(key);
    }

    /**
     * 递减
     */
    public static Long decrement(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForValue().decrement(key, delta);
    }

    /**
     * 递减
     */
    public static Double decrement(String key, double delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForValue().increment(key, -delta);
    }

    /**
     * 根据key和item获取hash值
     */
    public static Object getHash(String key, String item) {
        return REDIS_TEMPLATE.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     */
    public static Map<Object, Object> getHashItem(String key) {
        return REDIS_TEMPLATE.opsForHash().entries(key);
    }

    /**
     * 根据key保存hash
     */
    public static void setHash(String key, Map<String, Object> map) {
        REDIS_TEMPLATE.opsForHash().putAll(key, map);
    }

    /**
     * 根据key保存hash，并设置时间
     */
    public static void setHash(String key, Map<String, Object> map, long seconds) {
        RedisUtil.setHash(key, map);
        if (seconds > 0) {
            RedisUtil.setExpire(key, seconds);
        }
    }

    /**
     * 向一张hash表中放入数据，如果不存在将创建
     */
    public static void setHashValue(String key, String item, Object value) {
        REDIS_TEMPLATE.opsForHash().put(key, item, value);
    }

    /**
     * 向一张hash表中放入数据，如果不存在将创建，并设置时间
     */
    public static void setHashValue(String key, String item, Object value, long seconds) {
        RedisUtil.setHashValue(key, item, value);
        if (seconds > 0) {
            RedisUtil.setExpire(key, seconds);
        }
    }

    /**
     * 删除hash表中的值
     */
    public static void deleteHashItem(String key, Object... item) {
        REDIS_TEMPLATE.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     */
    public static Boolean hasHashItem(String key, String item) {
        return REDIS_TEMPLATE.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增
     */
    public static Long incrementHash(String key, String item, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForHash().increment(key, item, delta);
    }

    /**
     * hash递增
     */
    public static Double incrementHash(String key, String item, double delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return REDIS_TEMPLATE.opsForHash().increment(key, item, delta);
    }

    /**
     * hash递减
     */
    public static Long decrementHash(String key, String item, long delta) {
        return REDIS_TEMPLATE.opsForHash().increment(key, item, -delta);
    }

    /**
     * hash递减
     */
    public static Double decrementHash(String key, String item, double delta) {
        return REDIS_TEMPLATE.opsForHash().increment(key, item, -delta);
    }

    /**
     * 根据key获取Set中的所有值
     */
    public static Set<?> getSet(String key) {
        return REDIS_TEMPLATE.opsForSet().members(key);
    }

    /**
     * 根据value从一个set中查询,是否存在
     */
    public static Boolean hasSetKey(String key, Object value) {
        return REDIS_TEMPLATE.opsForSet().isMember(key, value);
    }

    /**
     * 将数据放入set缓存
     */
    public static void setSet(String key, Object... values) {
        REDIS_TEMPLATE.opsForSet().add(key, values);
    }

    /**
     * 将set数据放入缓存
     */
    public static void setSet(String key, long seconds, Object... values) {
        RedisUtil.setSet(key, values);
        if (seconds > 0) {
            RedisUtil.setExpire(key, seconds);
        }
    }

    /**
     * 获取set的长度
     */
    public static Long getSetSize(String key) {
        return REDIS_TEMPLATE.opsForSet().size(key);
    }

    /**
     * 移除set的value
     */
    public static void removeSetValue(String key, Object... values) {
        REDIS_TEMPLATE.opsForSet().remove(key, values);
    }

    /**
     * 获取list的内容
     */
    public static List<?> getList(String key, long start, long end) {
        return REDIS_TEMPLATE.opsForList().range(key, start, end);
    }

    /**
     * 获取list的长度
     */
    public static Long getListSize(String key) {
        return REDIS_TEMPLATE.opsForList().size(key);
    }

    /**
     * 通过索引获取list中的值
     */
    public static Object getListIndex(String key, long index) {
        return REDIS_TEMPLATE.opsForList().index(key, index);
    }

    /**
     * 向list尾部追加数据
     */
    public static void pushList(String key, Object value) {
        REDIS_TEMPLATE.opsForList().rightPush(key, value);
    }

    /**
     * 向list尾部追加数据
     */
    public static void pushList(String key, Object value, long seconds) {
        RedisUtil.pushList(key, value);
        if (seconds > 0) {
            RedisUtil.setExpire(key, seconds);
        }
    }

    /**
     * 向list尾部追加数据
     */
    public static void pushList(String key, List<?> value) {
        REDIS_TEMPLATE.opsForList().rightPushAll(key, value);
    }

    /**
     * 将list放入缓存
     */
    public static void pushList(String key, List<?> value, long seconds) {
        RedisUtil.pushList(key, value);
        if (seconds > 0) {
            RedisUtil.setExpire(key, seconds);
        }
    }

    /**
     * 根据索引修改list中的某条数据
     */
    public static void setListIndex(String key, long index, Object value) {
        REDIS_TEMPLATE.opsForList().set(key, index, value);
    }

}
